var PRINTTXT = "";
var POSTID = 0;
var FRIENDID = 0;
var appkey = "9eda9296a16c205c18bfd2e2b5f9f6bc";

class Friend {
	constructor(name) {
		this.name = name;
		this.friendlist = [];
		this.messagelist = [];
		this.postlist = [];
		FRIENDID += 1;
		this.id = String(FRIENDID);
	}

	getName() {
		return this.name;
	}
	
	getId() {
		return this.id;
	}

	getMessages() {
		var tmpstr = "Your messages: <br><br>";
		for (var i = 0; i < this.messagelist.length;i++) {
			tmpstr += "<strong>From: " + this.messagelist[i].sender.getName() + " To: " +
			this.messagelist[i].receiver.getName() + "</strong><br><em>" + this.messagelist[i].getContent() +
			"</em><br><br>";
		}

		return tmpstr;
	}

	printPosts() {
		if (this.postlist.length < 1)
			document.getElementById("print").innerHTML += "You have no posts yet:(";

		for (var i = 0; i < this.postlist.length;i++) {
			this.postlist[i].printPost();
		}
	}
}

class Post {
	constructor(poster, imgname, posttext, place, likers) {
		this.poster = poster;
		this.content = posttext;
		POSTID += 1;
		this.id = String(POSTID);
		this.imgname = imgname;

		this.likes = Math.floor(Math.random() * 10);
		this.donations = 0;
		this.likers = likers;
		this.donations = 0;
		this.place = place;
		this.wind = "press 'feed' for windspeeds";
		updateWindSpeed(this);
	}

	printPost() {
		document.getElementById("print").innerHTML += "<b>" + this.poster.getName() + "</b>" + "<span><i class='fas fa-map-marker-alt'></i>" + this.place +
					"<br></span><img src='resources/" + this.imgname +
					"' width='170' height='200'><br>" + "<span><i class='fas fa-wind'></i>" + this.wind +"</span><br>" +
					"<i onclick=addLike(" + this.id + ") class='fa fa-thumbs-up'></i> " +
					String(this.likes) + "  <i onclick='addDonation(" + this.id +
					", 1)' class='fa fa-money'></i> " + String(this.donations) + " €<br>" +
					this.getContent() + "<br><br><br>";
	}

	addLike(liker) {
		if (this.likers.includes(liker))
			alert("You already liked this post, you fool!:D");
		else {
			this.likes += 1;
			this.likers.push(liker);
		}
	}

	addDonation(amount) {
		this.donations += amount;
	}

	getContent() {
		return this.content;
	}
}

class Message {
	constructor(sender, receiver, msgtext) {
		this.sender = sender;
		this.receiver = receiver;
		this.content = msgtext;
	}

	printMsg() {
		PRINTTXT += "Lahetetty viesti: " + this.sender.getName() + " " +
		this.receiver.getName() + " = " + this.getContent() +
		"<br><br>";
	}

	getContent() {
		return this.content;
	}
}

class Service {
	constructor() {
		this.friends = [];
		this.messages = [];
		this.posts = [];
	}

	addFriend(friend) {
		this.friends.push(friend);
	}

	getFriend(name) {
		for (var i = 0; i < this.friends.length; i++) {
			if (this.friends[i].getName() == name)
				return this.friends[i];
		}
	}
	
	getFriendById(id) {
		for (var i = 0; i < this.friends.length; i++) {
			if (this.friends[i].getId() == id)
				return this.friends[i];
		}
	}

	makePals(friendA, friendB) {
		friendA.friendlist.push(friendB);
		friendB.friendlist.push(friendA);
	}
	sendMessage(sender, receiver, msgtxt) {
		if (this.friends.includes(receiver)) {
			const msg = new Message(sender, receiver, msgtxt);
			sender.messagelist.push(msg);
			receiver.messagelist.push(msg);
			this.messages.push(msg);
			return true;
		}
		else
			return false;
	}

	sendPost(poster, imgname, posttxt, place, likers) {
		const post = new Post(poster, imgname, posttxt, place, likers);
		poster.postlist.push(post);
		this.posts.push(post);
		updateJsonPosts();
	}

	addLikeToPost(id, ME) {
		for (var i = 0; i < this.posts.length; i++) {
			if (this.posts[i].id == id) {
				this.posts[i].addLike(ME);
				//console.log(String(this.posts[i].likes));
			}
		}
	}

	addDonuteToPost(id, amount) {
		for (var i = 0; i < this.posts.length; i++) {
			if (this.posts[i].id == id) {
				this.posts[i].addDonation(amount);
			}
		}
	}

	printAllMessages(id) {
		var tmpstr = "Kaikki viestit: <br>";
		for (var i = 0; i < this.messages.length;i++) {
			tmpstr += "Viesti:" + i + " " + this.messages[i].sender.getName() + " " +
			this.messages[i].receiver.getName() + " = " + this.messages[i].getContent() +
			"<br>";
		}

		document.getElementById(id).innerHTML = tmpstr;
	}

	printAllPosts(id) {
		var tmpstr = "<br><br>";
		document.getElementById("print").innerHTML = tmpstr;
		for (var i = this.posts.length - 1; i >= 0;i--) {
			this.posts[i].printPost();
		}
	}

	updateWindspeeds() {
		for (var i = 0; i < this.posts.length;i++) {
			updateWindSpeed(this.posts[i]);
			console.log("Worked");
		}
	}
}

//we initialize the users/friends here
const myservice = new Service();
const ME = new Friend("Annie");
const f = new Friend("Kalle");
const f2 = new Friend("Mikko");
const f3 = new Friend("Henri");
const f4 = new Friend("Jaakko");
const f5 = new Friend("Jami");

function initService() {
	myservice.addFriend(f);
	myservice.addFriend(f2);
	myservice.addFriend(f3);
	myservice.addFriend(f4);
	myservice.addFriend(f5);
	myservice.addFriend(ME);

	//laitetaan kaikille kaksi kaveria (symmetrinen)
	myservice.makePals(f,f5);
	myservice.makePals(f,f2);
	myservice.makePals(f2,f4);
	myservice.makePals(f3,f4);
	myservice.makePals(f3,f5);

	myservice.makePals(ME,f5);
	myservice.makePals(ME,f2);
	myservice.makePals(ME,f3);

	//some functions for general admin initialization
	
	/*initFriend(myservice,f,f5,"Hej på dej!");
	initFriend(myservice,f5,f,"Hey son!");
	initFriend(myservice,f2,f4,"Hey, I heard someone is talking to Kalle");
	initFriend(myservice,f4,f2,"Yeah, it's weird!:D");
	initFriend(myservice,f,f2,"Stop talking behind my back!xD");

	initFriend(myservice,ME,f5,"Hej på dej!");
	initFriend(myservice,f3,ME,"Hey, I heard someone is talking to Kalle");
	*/

	/*initPost(myservice, f, "logo.png", "This is a POST!", "Chicago",[]);
	initPost(myservice, f2, "old_mill.jpg", "This is the second post", "Amsterdam",[]);
	initPost(myservice, ME, "video_thumbnail.jpg", "This is my windmill", "Espoo",[]);*/
	
	
	//wait for api requests
	setTimeout(function(){
		myservice.printAllPosts("print");
	}, 1000);
	//myservice.updateWindspeeds();
	
	//request for service data from cloud
	getJsonMessages();
	getJsonPosts();
	
	myservice.updateWindspeeds();
}

function initFriend(service,a,b, msgtxt) {
	service.sendMessage(a,b,msgtxt);
}

function initPost(service, poster, imgname, posttxt, place, likers) {
	service.sendPost(poster, imgname, posttxt, place, likers);
}

function addLike(id) {
	myservice.addLikeToPost(id, ME);
	myservice.printAllPosts("print");
}

function addDonation(id, amount) {
	if (confirm("Donate 1€ for this post?")) {
		myservice.addDonuteToPost(id, amount);
		myservice.printAllPosts("print");
	}
}

//this function display the feed
function showFeed() {
	myservice.printAllPosts("print");
}

//this function displays the 'add post' screen
function showAddPost() {
	var tmpstr = "<b>Add a new Windstagram post: </b><br>";
	document.getElementById("print").innerHTML = tmpstr + "<input class='type-box'" +
			"type='text' id='post-txt' placeholder='Post text...'><br>" +
			"<input class='type-box' type='text' id='place-txt'" +
			"placeholder='Windmill location...'><br>" +
			"<input class='type-box' type='text' id='image-txt' " +
			"placeholder='Image name...'> <br>" +
			"<button class='post-button' type='button' onclick='addPost()'>POST </button>";
}

function addPost() {
	var posttxt = document.getElementById("post-txt").value;
	var place = document.getElementById("place-txt").value;
	var imgname = document.getElementById("image-txt").value;
	myservice.sendPost(ME, imgname, posttxt, place, []);
	alert("Post posted!");
	updateJsonPosts();
}

function showMessages() {
	document.getElementById("print").innerHTML = ME.getMessages() +
			"<br>New message?<br>" + "<input class='type-box'" +
			"type='text' id='msg-txt' placeholder='Type your message here...'><br>" +
			"<input class='type-box' type='text' id='receiver-txt' " +
			"placeholder='Type receiver name here...'> <br>" +
			"<button class='post-button' type='button' onclick='sendMessage()'>Send message </button>";
}

function sendMessage() {
	var msg = document.getElementById("msg-txt").value;
	var receiver = document.getElementById("receiver-txt").value;
	frd = myservice.getFriend(receiver);
	if (myservice.sendMessage(ME,frd,msg))
		alert("Message sent!");
	else
		alert("Problem sending message!");
	showMessages();
	updateJsonMessages()
}

function showProfile() {
	document.getElementById("print").innerHTML = "<span><img src='resources/woman-profile.jpg'" +
			"width='150'></span><br>";
	document.getElementById("print").innerHTML += "<b>Annie Walker</b><br>annie.walker@gmail.com<br><br><br>";
	document.getElementById("print").innerHTML += "<b>Your Posts</b><br><br>"
	ME.printPosts();

}

function showInfo() {
	document.getElementById("print").innerHTML = "<br><br><b>What is Windstagram?</b><br><br><br>"+
	"Windstagram is a social media platform that helps you to give some love to the <br>" +
												"world and bring awareness to wind power. Add your favourite wind mill picture <br>" +
												"to windstagram and show your friends how much hidden energy is  around you. <br><br>"
												+"Your friends can like your post and donate money to the charity to build a new <br>"+
												"wind power plant . You can also send direct messages to other <br>"+
												"windstagrammers and talk about wind and meet up!<br><br><br><br>"+
												"<b>Instructions</b><br><br><br>"+
												"<b>1.Feed</b><br><br>"+
												"From here you can see your friends posts, the wind speed of their location<br> and donate money to their windmill <br><br>"+
												"<b>2.Profile</b><br><br>"+
												"Here you can see your own profile, your personal info,<br>"+
												"how much money you have raised and your own posts.<br><br>"+
												"<b>3. Add post</b><br><br>"+
												"From ”Add post” you can add a new post. You do that by adding a <br>"+
												"descriptive text, the location of your windmill and the image. If the post <br>"+
												"has been successful you will get an notification.<br><br>"+
												"<b>4. Messages</b><br><br>"+
												"From here you can see the messages you have received and send new<br> "+
												"messages to your friends!<br><br>";
}

//fetching new the most recent windspeeds from api
function updateWindSpeed(post) {
	searchInput = post.place;

	var searchLink = "https://api.openweathermap.org/data/2.5/weather?q=" + searchInput + "&appid="+ appkey;
	var request = new XMLHttpRequest();
	request.open('GET', searchLink);
	request.send();
	request.onload = function() {
		//console.log(request.response);
		var jsonObject = JSON.parse(request.responseText);
		post.wind = String(jsonObject.wind.speed) + " m/s";
	};
}

function getJsonMessages() {
	var searchLink = "https://api.myjson.com/bins/rn8x6";
	var request = new XMLHttpRequest();
	request.open('GET', searchLink);
	request.send();
	request.onload = function() {
		console.log(request.response);
		var jsonObject = JSON.parse(request.responseText);
		setTimeout(function() {
			for (var i = 0; i < jsonObject["messages"].length; i++) {
				sender = myservice.getFriendById(Number(jsonObject["messages"][i].senderid));
				receiver = myservice.getFriendById(Number(jsonObject["messages"][i].receiverid));
				content = jsonObject["messages"][i].content;
				initFriend(myservice,sender,receiver,content);
			}
		}, 100);
	};
}

function updateJsonMessages() {
	var url = "https://api.myjson.com/bins/rn8x6";
	
	tmpdict = {};
	tmplist = [];
	for (var i = 0; i < myservice.messages.length; i++) {
		var data = {};
		data.senderid = myservice.messages[i].sender.id;
		data.receiverid = myservice.messages[i].receiver.id;
		data.content = myservice.messages[i].content;
		tmplist.push(data);
	}
	tmpdict.messages = tmplist;
	
	var json = JSON.stringify(tmpdict);

	var xhr = new XMLHttpRequest();
	xhr.open("PUT", url, true);
	xhr.setRequestHeader('Content-type','application/json; charset=utf-8');
	xhr.onload = function () {
		var users = JSON.parse(xhr.responseText);
		if (xhr.readyState == 4 && xhr.status == "200") {
			console.table(users);
		} else {
			console.error(users);
		}
	}
	
	xhr.send(json);
}

function getJsonPosts() {
	var searchLink = "https://api.myjson.com/bins/14va9m";
	var request = new XMLHttpRequest();
	request.open('GET', searchLink);
	request.send();
	request.onload = function() {
		console.log(request.response);
		var jsonObject = JSON.parse(request.responseText);
		setTimeout(function() {
			
			for (var i = 0; i < jsonObject["posts"].length; i++) {
				poster = myservice.getFriendById(Number(jsonObject["posts"][i].posterid));
				content = jsonObject["posts"][i].content;
				imgname = jsonObject["posts"][i].imgname;
				likes = jsonObject["posts"][i].likes;
				donations = jsonObject["posts"][i].donations;
				place = jsonObject["posts"][i].place;
				wind = jsonObject["posts"][i].wind;
				likers = jsonObject["posts"][i].likers;
				initPost(myservice, poster, imgname, content, place, likers);
			}
		}, 100);
	};
}

function updateJsonPosts() {
	var url = "https://api.myjson.com/bins/14va9m";
	
	tmpdict = {};
	tmplist = [];
	for (var i = 0; i < myservice.posts.length; i++) {
		var data = {};
		data.posterid = myservice.posts[i].poster.id;
		data.content = myservice.posts[i].content;
		data.imgname = myservice.posts[i].imgname;
		data.likes = myservice.posts[i].likes;
		data.donations = myservice.posts[i].donations;
		data.place = myservice.posts[i].place;
		data.wind = myservice.posts[i].wind;
		data.likers = [];
		for (var j = 0; j < myservice.posts[i].likers; j++) {
			data.likers.push(myservice.getFriendById(myservice.posts[i].likers[j].getId()))
		}
		
		
		tmplist.push(data);
	}

	tmpdict.posts = tmplist;
	var json = JSON.stringify(tmpdict);

	var xhr = new XMLHttpRequest();
	xhr.open("PUT", url, true);
	xhr.setRequestHeader('Content-type','application/json; charset=utf-8');
	xhr.onload = function () {
		var posts = JSON.parse(xhr.responseText);
		if (xhr.readyState == 4 && xhr.status == "200") {
			console.table(posts);
		} else {
			console.error(posts);
		}
	}
	
	xhr.send(json);
}

